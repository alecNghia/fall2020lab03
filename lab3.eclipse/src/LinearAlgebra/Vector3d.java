package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d (double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {		
		return Math.sqrt(x * x + y* y + z * z );
	}
	
	public double dotProduct(Vector3d secondVec) {
		return (x * secondVec.getX() + y * secondVec.getY() + z * secondVec.getZ());
	}
	
	public Vector3d add(Vector3d secondVec) {
		double newX = x + secondVec.getX();
		double newY = y + secondVec.getY();
		double newZ = z + secondVec.getZ();
		Vector3d newVec = new Vector3d(newX,newY,newZ);
		return newVec;
	}
}
