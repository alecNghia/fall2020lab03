package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
    
    void getTest() {
        Vector3d get = new Vector3d(11, 22, 33);
        assertEquals(11, get.getX());
        assertEquals(22, get.getY());
        assertEquals(33, get.getZ());

    }
    
    @Test
    
    void magnitudeTest() {
        Vector3d mag = new Vector3d(4, 5, 6);
        assertEquals(8.774964387392123, mag.magnitude());
        
    }
    
    @Test
    
    void dotProductTest() {
        Vector3d dotOne = new Vector3d(1, 2, 3);
        Vector3d dotTwo = new Vector3d(2, 3, 4);
        assertEquals(20.0, dotOne.dotProduct(dotTwo));
        
    }
    
    @Test
    
    void addTest() {
        Vector3d addOne = new Vector3d(2, 4, 6);
        Vector3d addTwo = new Vector3d(6, 8, 10);
        Vector3d newAdd = addOne.add(addTwo);
        assertEquals(8, newAdd.getX());
        assertEquals(12, newAdd.getY());
        assertEquals(16, newAdd.getZ());
        
    }

}